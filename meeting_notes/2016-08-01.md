# August 1st, 2016

Students started working on these new issues:

* https://gitlab.com/gitlab-org/gitlab-ce/issues/20321

Other than that they're still working on last week's issues. The following merge
requests exist for these issues:

* https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/5496
* https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/5389
